const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();

const port = 8000;

const secret = 'key';

app.get('/encode/:password', (req, res) => {
    const encryptedMessage = Vigenere.Cipher(secret).crypt(req.params.password);
    res.send(`Your encrypted password is ${encryptedMessage}`);
});


app.get('/decode/:encrypted', (req, res) => {
    const message = Vigenere.Decipher(secret).crypt(req.params.encrypted);
    res.send(`Your encrypted password is ${message}`)
});


app.listen(port, () => {
    console.log('we are live on' + port);
});